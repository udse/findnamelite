# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Jianwei Wu's Research Project for detecting bad test names
with Intellij Plugin Implementation

### How do I get set up? ###
* Import the project from Bitbucket
* Restore all dependencies from "libs" folder
* Change the project name difference in all absolute paths
* Run the plugin

### Who do I talk to? ###
Dr. James Clause

### Program Structure ###
1. Test Name Patterns
1. Test Body Patterns
1. Comparison

### Notes ###
* Using IntelliJ Idea 2017.3.5 for running the project and modifications is preferred.
* Newer version of Idea is supported (IntelliJ Idea Ultimate 2018.2).